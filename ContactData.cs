﻿using System;

namespace Kontaktdatenverwaltung_A
{
    class ContactData
    {
        public static Type[] Types = new Type[]{
            typeof(PostalAddress),
            typeof(PhoneNumber),
            typeof(EmailAddress),
        };
        public virtual void Print()
        {
            // TODO: Implement
        }
    }
}