﻿namespace Kontaktdatenverwaltung_A
{
    class EmailAddress : ContactData
    {
        public string Email { get; set; }

        public override void Print()
        {
            System.Console.WriteLine(Email);
        }
    }
}