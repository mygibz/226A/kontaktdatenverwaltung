﻿namespace Kontaktdatenverwaltung_A
{
    class ExternalPerson : Person
    {
        public string Company { get; set; }

        public new void Print()
        {
            base.Print();
            System.Console.WriteLine($"Company: {Company}");
        }
    }
}