﻿using System;

namespace Kontaktdatenverwaltung_A
{
    class InternalPerson : Person
    {
        public string Department { get; set; }
        public DateTime EntryDate { get; set; }

        public new void Print()
        {
            base.Print();
            Console.WriteLine($"Deparment: {Department}");
            Console.WriteLine($"EntryDate: {EntryDate:dd.MM.yyyy}");
        }
    }
}