﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Kontaktdatenverwaltung_A
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public Gender Gender { get; set; }

        private List<ContactData> _contactData = new List<ContactData>();

        private ContactData[] _primaryContactData = new ContactData[ContactData.Types.Length];

        public ContactData AddContactData(ContactData data)
        {
            // TODO: Prüfen, ob data als Primär-Information gespeichert werden muss...!

            _contactData.Add(data);
            return data;
        }

        public ContactData RemoveContactData(ContactData data)
        {
            // TODO: Was ist, wenn die entfernte Kontakt-Information Primär-Information war?

            // Notice: Will remove only IDENTICAL instances
            _contactData.Remove(data);
            return data;
        }

        private ContactData SetPrimary(ContactData primary)
        {
            // Enforece SUBSET restriction
            if (!_contactData.Contains(primary))
            {
                _contactData.Add(primary);
            }

            int indexOfContactDataType = Array.IndexOf(ContactData.Types, primary.GetType());
            _primaryContactData[indexOfContactDataType] = primary;
            return primary;
        }

        public PhoneNumber SetPrimaryPhoneNumber(PhoneNumber primary)
        {
            SetPrimary(primary);
            return primary;
        }

        public PostalAddress SetPrimaryPostalAddress(PostalAddress primary)
        {
            SetPrimary(primary);
            return primary;
        }

        public EmailAddress SetPrimaryEmailAddress(EmailAddress primary)
        {
            SetPrimary(primary);
            return primary;
        }

        public virtual void Print()
        {
            Console.WriteLine("\n");
            Console.WriteLine($"***** {FirstName} {LastName} *****");
            Console.WriteLine($"Gender:     {Gender}");
            Console.WriteLine($"Birthday:   {Birthday:dd.MM.yyyy}");

            // Print contact data
            foreach (ContactData contactData in _contactData)
            {
                if (Array.IndexOf(_primaryContactData, contactData) >= 0)
                {
                    Console.Write(" (*) ");
                }
                contactData.Print();
            }
        }
    }
}