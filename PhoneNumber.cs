﻿namespace Kontaktdatenverwaltung_A
{
    class PhoneNumber : ContactData
    {
        public string Number { get; set; }
        public PhoneNumberType Type { get; set; }

        public override void Print()
        {
            System.Console.WriteLine($"{Number} ({Type})");
        }
    }
}