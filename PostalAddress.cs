﻿namespace Kontaktdatenverwaltung_A
{
    class PostalAddress : ContactData
    {
        public string Street { get; set; }
        public int ZipCode { get; set; }
        public string City { get; set; }

        public override void Print()
        {
            System.Console.WriteLine($"{Street}, {ZipCode} {City}");
        }

    }
}