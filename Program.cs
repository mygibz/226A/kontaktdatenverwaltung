﻿using System;
using System.Collections.Generic;

namespace Kontaktdatenverwaltung_A
{
    class Program
    {
        static void Main(string[] args)
        {

            Person person = new Person { FirstName = "Albert", LastName = "Einstein", Gender = Gender.MALE, Birthday = new DateTime(1879, 03, 14) };
            InternalPerson internalPerson = new InternalPerson { FirstName = "Marie", LastName = "Curie", Gender = Gender.FEMALE, Birthday = new DateTime(1920, 6, 3), Department = "Chemistry", EntryDate = new DateTime(1940, 8, 1) };
            ExternalPerson externalPerson = new ExternalPerson { FirstName = "Donald", LastName = "Duck", Gender = Gender.MALE, Birthday = new DateTime(1950, 7, 4), Company = "Entenhausen AG" };

            #region DEMO
            /*
            List<Person> persons = new List<Person>();
            
            persons.Add(person);
            persons.Add(internalPerson);
            persons.Add(externalPerson);

            foreach (Person p in persons)
            {
                p.Print();
            }
            */
            #endregion DEMO

            person.Print();
            internalPerson.Print();
            externalPerson.Print();

            EmailAddress email1 = new EmailAddress { Email = "marie@genio.us" };
            ContactData email2 = new EmailAddress { Email = "madamecurie@gibz.ch" };
            PhoneNumber phoneNumber = new PhoneNumber { Number = "041 228 33 49", Type = PhoneNumberType.LANDLINE };

            internalPerson.AddContactData(email1);
            internalPerson.SetPrimaryPhoneNumber(phoneNumber);

            internalPerson.Print();
        }
    }
}