# Sind wir alle gleich? (Gleichheit)

**Was was ist eigentlich *gleich*? Gibt es Dinge im Leben einer Programmiererin, die *gleicher* sind als andere?**

Keine Angst - Sie sind *nicht* in einer Philosophie-Lektion gelandet! Im Rahmen dieser Lernsequenz werden Sie sich selbständig eine programmierspezifische Antwort auf diese Frage erarbeiten. Dabei sind Ihnen einige Ressourcen vorgegeben. Sie dürfen und sollen während der Bearbeitung dieser Unterlagen durchaus auch *zusätzliche* Unterlagen und Quellen konsultieren. Wie tief Sie sich in dieses Problem eingraben, bleibt letztendlich Ihnen überlassen. Als Leitlinien sollen Ihnen die folgenden Lernziele dienen.

## Lernziele

Mit der Bearbeitung dieser Lernsequenz sollen Sie folgende Ziele erreichen:

1. Sie wissen grundsätzlich, wofür die Methode `Equals` in C# gebraucht wird.
2. Sie kennen die beiden Konzepte **Äquivalenz** (auch: *equivalence* oder *value equality*) und **Identität** (auch: *identity* oder *reference equality*).
3. Sie verstehen die fünf *Garantien*, welche jede Implementation der *Äquivalenz* erfüllen sollen.
4. Sie können für konkrete Anwendungsfälle begründet entscheiden, ob Sie **Äquivalenz** oder **Identität** als Konzept für die Gleichheit zweier Objekte einsetzen.
5. Sie können für eigene Klassen die Methode `Equals` korrekt und effizient überschreiben.
6. Sie wissen grundsätzlich, wofür die Methode `GetHashCode` in C# gebraucht wird und können Beispiele nennen, wo diese Methode implizit (d.h. meist durch *andere* Klassen) oder explizit (d.h. meist durch die *eigenen* Klassen) aufgerufen wird.
7. Sie können den inhaltlichen Zusammenhang der beiden Methoden `Equals` und `GetHashCode` in C# erklären.
8. Sie können für eigene Klassen die Methode `GetHashCode` (basierend auf einer Implementation der Methode `Equals`) korrekt und effizient implementieren.
9. Sie kennen den funktionalen Zusammenhang der Methode `Equals` und des Vergleichsoperators `==`.
10. Sie wissen grundsätzlich, wie Operatoren für eine spezifische Klassen überschrieben werden können.
11. Sie können den Vergleichsoperator `==` (basierend auf der entsprechenden Implementation der Methode `Equals`) korrekt und effizient implementieren.
12. Sie Verstehen, weshalb eine Überschreibung des Vergleichsoperators `==` in jedem Fall zu einer Überschreibung des Vergleichsoperators `!=` führen muss (und umgekehrt).
13. Sie kennen eine effiziente Lösung, um bei gegebener Überschreibung des Vergleichsoperators `==` die Implementation des inversen Operators `!=` zu implementieren.

## Vorgehen

Sie sollen während der zur Verfügung stehenden Unterrichtszeit selbständig sicherstellen, dass Sie alle oben genannten Lernziele erreichen. Dazu steht Ihnen der untenstehende Katalog an Ressourcen zur Verfügung, welche Sie zur Erreichung der Lernziele einsetzen können (und sollen). Es ist selbstverständlich erlaubt, weitere, ergänzende Ressourcen zu nutzen.

Wenn Sie während dieser Lernsequenz auf Probleme oder Fragen stossen, welche Sie auch durch ergänzende, alternative Ressourcen nicht in nützlicher Frist selbständig lösen können, schreiben Sie diese Frage in den Kanal *M120* in *Teams*. Gleichzeitig Sind Sie aufgefordert, offene Fragen in diesem Kanal zu beantworten bzw. Hinweise für die Lösung des Problems zu geben.

Neben der Bearbeitung der nachfolgend genannten Ressourcen wird dringend empfohlen, die Inhalte konkret und praktisch anzuwenden. Dazu eignet sich die zuvor erstellte Applikation *Kontaktdatenverwaltung* optimal. Die darin enthaltene Klasse `Person` und insbesondere die verschiedenen Subklassen von `ContactData` eignen sich sehr gut für die Implementierung der Methoden `Equals`, `GetHashCode` sowie für die Überschreibung der beiden Vergleichsoperatoren `==` und `!=`.

## Ressourcen

Die Verwendung der nachfolgenden Ressourcen wird für die effiziente Bearbeitung dieser Lernsequenz empfohlen. Diese Ressourcen sind spezifisch für die Erreichung der oben genannten Lernziele ausgewählt.

### Dreiteilige Anleitung zur Überschreibung von `Equals`, `GetHashCode` sowie der damit verwandten Vergleichsoperatoren

Im November 2014 hat der Amerikaner *Logan Franken* in seinem Blog beschrieben (und mehrfach aktualisiert), wie in C# die genannten Methoden grundsätzlich funktionieren und überschrieben werden können. In [Teil 1](https://www.loganfranken.com/blog/698/overriding-equals-in-c-part-3/) erklärt er schrittweise, wie die `Equals` Methode zur Implementation der *Wertegleichheit* überschrieben werden kann. Im [Teil 2](http://www.loganfranken.com/blog/692/overriding-equals-in-c-part-2/) wird gezeigt, inwiefern die Methode `GetHashCode` von der Implementation der Methode `Equals` abhängt und entsprechend überschrieben werden kann. Im [Teil 3](https://www.loganfranken.com/blog/698/overriding-equals-in-c-part-3/) wird schliesslich erklärt, wie die beiden Vergleichsoperatoren `==` und `!=` überschrieben werden können.

> Es lohnt sich, alle drei Teile dieser Serie aufmerksam und sorgfältig zu lesen und parallel dazu anhand des Projekts *Kontaktdatenverwaltung* umzusetzen.

### *Gleichheit* erklärt im *C# Programming Guide* von Microsoft

Im Abschnitt [Equality Comparisons](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/equality-comparisons) des offiziellen C# Programming Guide werden die Unterschiede zwischen *Äquivalenz* und *Identität* erklärt und anhand von Code-Beispielen aufgezeigt.

> In diesem (kürzeren) Artikel werden einige theortische Grundlagen, welche auch im Teil 1 von Logan Frankens Serie enthalten sind, nochmals verdeutlicht und mit weiteren Code-Beispielen illustiert.

### Anleitung zur Implementierung von Äquivalenz

In einem spezifischen Abschnitt des oben genannten Artikels (*Gleichheit* erklärt im *C# Programming Guide* von Microsoft) wird die [Definition und Implementation der Äquivalenz](https://docs.microsoft.com/en-us/dotnet/csharp/programming-guide/statements-expressions-operators/how-to-define-value-equality-for-a-type) spezifisch ausgeführt.

> Relevant in dieser Ressource sind primär die *5 Garantien*, welche jede Implementation der Äquivalenz erfüllen sollen. Diese sind gleich am Anfang des Abschnittes zu finden.

> Achtung: In diesem Teil des *C# Programming Guide* sind teilweise technische Informationen enthalten, welche aufgrund der bisherigen Inhalte Ihrer Ausbildung evt. noch unverständlich sind - lassen Sie sich davon nicht irritieren. Sie müssen (noch) nicht alles verstehen. Es reicht für die Erreichung der Lernziele grundsätzlich, wenn Sie den ersten Teil (5 Garantien) verstanden haben.

### Applikation *Kontaktdatenverwaltung*

Für die praktische Anwendung der Inhalte eignet sich die Applikation *Kontaktdatenverwaltung* aus dem Modul 226A sehr gut.

> Sie können dafür vorzugsweise Ihre (überarbeitete) Version dieser Applikation verwenden. Alternativ steht Ihnen der gemeinsam erarbeitete Quellcode aus der letzten Unterrichtseinheit in den *Dateien* auf *Teams* zur Verfügung.

